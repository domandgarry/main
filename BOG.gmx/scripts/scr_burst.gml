///scr_melee()

//Play swing animation
if (mouse_check_button(mb_left) && toolIndex = 0 && burst > 0) {
    toolXScale *= 2;
    toolYScale *= 2;
    toolPlay = true;
    var o = instance_create(x,y,obj_bullet);
    o.direction = angle;
    o.image_angle = angle;
    o.speed = 15;
    alarm[2] = 5;
}

if burst = 0 && timer[3] = -1 {
    timer[3] = 30;
}
