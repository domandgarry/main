///scr_light(size,color)
var size = argument[0]/2;
var color = argument[1];
draw_set_blend_mode(bm_subtract);
surface_set_target(light);
draw_ellipse_color(x-size-view_xview,y-size-view_yview,x+size-view_xview,y+size-view_yview,color,c_black,false);
surface_reset_target();
draw_set_blend_mode(bm_normal);
