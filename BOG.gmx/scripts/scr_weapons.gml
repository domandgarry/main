
/*
Create array of weapons
weapon[0,0] = ""; //Name of weapon
weapon[0,1] = 0; //Firerate (0,1,2,3,4,5) (Melee, single, burst, auto, lazer, shotgun)
weapon[0,2] = 0; //Bullet Type (0,1,2,3) (N/A, Bullet, Shell, Energy)
weapon[0,3] = spr_gun; //sprite name
weapon[0,4] = 0; //Weight of weapon (0,1,2) (Light, Medium, Heavy)
weapon[0,5] = 0; //Firerate/Animation speed
*/

weapon[0,0] = "Fists";
weapon[0,1] = 0; 
weapon[0,2] = 0;
weapon[0,3] = spr_weapon_fists;
weapon[0,4] = 0;
weapon[0,5] = 0.3;

weapon[1,0] = "SMG";
weapon[1,1] = 3; 
weapon[1,2] = 1;
weapon[1,3] = spr_weapon_auto;
weapon[1,4] = 1;
weapon[1,5] = 0.3;

weapon[2,0] = "Burst Rifle";
weapon[2,1] = 2; 
weapon[2,2] = 1;
weapon[2,3] = spr_weapon_burst;
weapon[2,4] = 1;
weapon[2,5] = 0.1;

weapon[3,0] = "Laser Gun";
weapon[3,1] = 4; 
weapon[3,2] = 3;
weapon[3,3] = spr_weapon_laser;
weapon[3,4] = 2;
weapon[3,5] = 0.05;

weapon[4,0] = "Shotgun";
weapon[4,1] = 4; 
weapon[4,2] = 2;
weapon[4,3] = spr_weapon_shotgun;
weapon[4,4] = 2;
weapon[4,5] = 0.08;


