///scr_shotgun()

//Play swing animation
if mouse_check_button(mb_left) && toolIndex = 0 {
    toolXScale *= 2;
    toolYScale *= 2;
    toolPlay = true;
    for(var i=0;i<5;i++) {
        var o = instance_create(x,y,obj_bullet);
        var rand = random_range(-20,20);
        o.direction = angle+rand;
        o.image_angle = angle+rand;
        o.speed = 15;
    }
}
