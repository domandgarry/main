//Basic enemy ai

switch(action) {
    case 0: //Stand still
        image_xscale *= -1;
        image_speed = 0;
        image_index = 0;
        speed = 0;
    break;
    case 1: //Move randomly
        var randomX = random_range(-20,20);
        var randomY = random_range(-20,20);
        move_towards_point(x+randomX,y+randomY,moveSpeed);
        if x > x+randomX {
            image_xscale = -1;
        } else {
            image_xscale = 1;
        }
        image_speed = 0.3;
    break;
    case 2: //Chase player
        if distance_to_object(obj_player) < 50 {
            chasePlayer = true;
        }
        move_towards_point(obj_barrel.x,obj_barrel.y,moveSpeed/2);
        image_speed = 0.3;
    break;
}
