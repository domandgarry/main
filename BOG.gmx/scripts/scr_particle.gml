///scr_particle(x,y,type,speed,amount,radius)
var posx = argument[0];
var posy = argument[1];
var type = argument[2];
var spd = argument[3];
var amount = argument[4];
var radius = argument[5];

for(var i=0;i<amount;i++) {
    switch(type) {
        case 0: //Bullet trail
            var o = instance_create(posx+random_range(-radius,radius),posy+random_range(-radius,radius),obj_particle);
            o.direction = image_angle-180;
            o.image_angle = image_angle-180;
            o.sprite_index = spr_pt_bullet;
            o.scale = random_range(0.25,0.75);
        break;
        case 1: //Walking
            var o = instance_create(posx+random_range(-radius,radius),posy+random_range(-radius,radius),obj_particle);
            o.direction = image_angle-180;
            o.image_angle = image_angle-180;
            o.sprite_index = spr_pt_dusttrail;
            o.scale = random_range(0.25,0.75);
        break;
        case 2: //Enemy hit
            var o = instance_create(posx+random_range(-radius,radius),posy+random_range(-radius,radius),obj_particle);
            o.direction = random(360);
            o.image_angle = random(360);
            o.speed = spd;
            o.sprite_index = spr_pt_blood;
            o.scale = random_range(0.75,1.25);
        break;
        case 3: //Explosion
            var o = instance_create(posx+random_range(-radius,radius),posy+random_range(-radius,radius),obj_particle);
            o.direction = random(360);
            o.image_angle = random(360);
            o.speed = spd;
            o.sprite_index = spr_pt_explosion;
            o.scale = random_range(1.25,1.75);
        break;
        case 4: //Wall hit
            var o = instance_create(posx+random_range(-radius,radius),posy+random_range(-radius,radius),obj_particle);
            o.direction = random(360);
            o.image_angle = random(360);
            o.speed = spd;
            o.sprite_index = spr_pt_dusttrail;
            o.scale = random_range(1.25,1.75);
        break;
    }
}
